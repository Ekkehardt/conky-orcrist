conky-orcrist
=============
Conky-Theme für meinen Laptop "selipnir", ehemals "orcrist".

Basierend auf Gotham (http://psyjunta.deviantart.com/art/Gotham-Conky-config-205465419), Conky Seamod (http://seajey.deviantart.com/art/Conky-Seamod-v0-1-283461046) und TeejeeTech NVIDIA Panel (http://www.teejeetech.in/2013/05/my-conky-themes-2.html) unter Verwendung von Hack 2.0 (http://sourcefoundry.org/hack).

Installation
------------
Alle Dateien nach `~/.conky/sleipnir` kopieren.

Start mittels Shellskript (auch als Autostart):

    sleep 3s
    killall conky
    cd "/home/USER/.conky/orcrist"
    conky -c "/home/USER/.conky/sleipnir/orcrist-time" &
    conky -c "/home/USER/.conky/selipnir/orcrist-system" &

Lizenz
------
GNU GPL V3
